FROM cimg/openjdk:17.0-browsers

# install required deps
RUN sudo apt-get update && \
	sudo apt-get install -y rsync locales curl
RUN sudo apt-get dist-upgrade

# install GNU parallel latest version
RUN curl pi.dk/3/ | sudo bash

# configure locale to workaround gradle crashes, cf. https://github.com/gradle/gradle/issues/3117#issuecomment-351376178
RUN sudo localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

ENV LANG en_US.utf8
ENV LC_ALL en_US.utf8
