# Docker images for URGI GnpIS projects

## Docker files

### For GnpIS Flower

This docker image relies on [Node official image](https://hub.docker.com/_/node) and includes:

- ImageMagick
- rsync

This image is available on <https://forgemia.inra.fr/urgi-is/docker-rare/container_registry/133>.

### For data-discovery & Faidare projects, at least

This is a docker image relying on [CircleCI cimg/openjdk:11.0-browsers](./Dockerfile) used by Gitlab CI to build the data-discovery and Faidare projects, including:

- a Java base image
- a Chrome binary
- a Firefox binary
- curl
- rsync (for data file synchronization)
- GNU parallel (for indexing data)

This image is available on <https://forgemia.inra.fr/urgi-is/docker-rare/container_registry/132>.

## How-to build

### Pre-requisites

Docker must be installed and running, look at the [official documentation](https://docs.docker.com/install/#supported-platforms) according to your environment.

### Build the image

#### Bash/git/git-lfs/openssh-client image

This image is used for deploying DataDiscovery synonyms into Elasticsearch cluster.

```sh
# For ForgeMIA
$ docker build -t registry.forgemia.inra.fr/urgi-is/docker-rare/bash-git-lfs:latest -f Dockerfile-bash-git-lfs .
```

#### Docker RARe

Once the DockerFile is modified, rebuild the Docker image from the `docker-rare` directory:

```sh
# For ForgeMIA
$ docker build -t registry.forgemia.inra.fr/urgi-is/docker-rare/docker-browsers:latest .
...
```

#### Docker GnpIS-Flower

```sh
# For ForgeMIA
$ docker build -t registry.forgemia.inra.fr/urgi-is/docker-rare/docker-gnpis-flower  -f Dockerfile-gnpis-flower .
...
```

#### Docker docker-git

```sh
# For ForgeMIA
$ docker build -t registry.forgemia.inra.fr/urgi-is/docker-rare/docker-git:latest -f Dockerfile-docker-git .
...
```

## Publish docker images

Log into your ForgeMIA account:

```sh
$ docker login registry.forgemia.inra.fr/urgi-is/docker-rare -u <your ForgeMIA username>
Authenticating with existing credentials...

Login Succeeded
```

Or into you Docker Hub account:

```sh
$ docker login -u <your docker hub id>
...
```

You must be part of the [`urgi` organization](https://hub.docker.com/u/urgi/) into Docker Hub.

Then push the images to ForgeMIA:

```sh
$ docker push registry.forgemia.inra.fr/urgi-is/docker-rare/bash-git-lfs:latest

$ docker push registry.forgemia.inra.fr/urgi-is/docker-rare/docker-browsers:latest
...
$ docker push registry.forgemia.inra.fr/urgi-is/docker-rare/docker-gnpis-flower:latest
...
$ docker push registry.forgemia.inra.fr/urgi-is/docker-rare/docker-git:latest
...
```
Example:

```sh
# For ForgeMIA
$ docker push registry.forgemia.inra.fr/urgi-is/docker-rare/docker-git:latest
The push refers to repository [registry.forgemia.inra.fr/urgi-is/docker-rare/docker-git]
8636ebb4c6d8: Pushed
45e396ca97b7: Pushed
062623b45873: Pushed
da8c28465f82: Pushed
d1292e236c36: Pushed
8890c171a12b: Pushed
4ae5bd84d450: Pushed
723a561964c8: Pushed
78764725b56c: Pushed
51d6d3d09136: Pushed
1507bc794612: Pushed
50644c29ef5a: Pushed
latest: digest: sha256:c0b7ce7d37d8ceebb16305410eb22a4d34dc09d8146878823a2cc4e61289c958 size: 2826

```

And *voilà*!
